/*
 * Task_023.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#include "Task_023.h"

#include <math.h>
#include <numeric>
#include <vector>
#include <set>
#include <algorithm>

Task_023::Task_023() :
		EulerTask(23) {
}

void Task_023::setup() {
}

/*
 * https://projecteuler.net/problem=23
 * A perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
 * For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28,
 * which means that 28 is a perfect number.
 * A number n is called deficient if the sum of its proper divisors is less than n
 * and it is called abundant if this sum exceeds n.
 * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16,
 * the smallest number that can be written as the sum of two abundant numbers is 24.
 * Let's call it 'special' number.
 * By mathematical analysis, it can be shown that all integers greater than 28123
 * can be written as the sum of two abundant numbers.
 * However, this upper limit cannot be reduced any further by analysis even though it is known
 * that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
 *
 * Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
 */

std::set<unsigned> getDivisors(unsigned n, bool properOnly = true) {
	std::set<unsigned> divisors;
	for (unsigned i = 1; i <= std::sqrt(n); i++) {
		if (n % i == 0) {
			divisors.insert(i);
			divisors.insert(n / i);
		}
	}
	if (properOnly)
		divisors.erase(n);
	return divisors;
}

unsigned getProperDivisorsSum(unsigned n) {
	std::set<unsigned> divisors = getDivisors(n);
	return std::accumulate(divisors.begin(), divisors.end(), 0);
}

bool isAbundant(unsigned n) {
	return getProperDivisorsSum(n) > n;
}

std::string Task_023::task() {
	const unsigned limit = 28123;
	unsigned final_sum = 0;
	std::vector<unsigned> integer_numbers(limit);
	std::set<unsigned> abundant_numbers;
	std::set<unsigned> special_numbers;
	std::set<unsigned> sets_diff;

	std::iota(std::begin(integer_numbers), std::end(integer_numbers), 1);

	for (auto i : integer_numbers)
		if (isAbundant(i))
			abundant_numbers.insert(i);

	for (auto a : abundant_numbers)
		for (auto b : abundant_numbers)
			special_numbers.insert(a + b);

	std::set_difference(integer_numbers.begin(), integer_numbers.end(),
			special_numbers.begin(), special_numbers.end(),
			std::inserter(sets_diff, sets_diff.begin()));

	final_sum = std::accumulate(sets_diff.begin(), sets_diff.end(), 0);

	return std::to_string(final_sum);
}

void Task_023::cleanup() {
}
