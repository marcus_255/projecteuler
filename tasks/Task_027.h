/*
 * Task_027.h
 *
 *  Created on: Feb 20, 2020
 *      Author: marcus
 */

#ifndef TASK_027_H_
#define TASK_027_H_
#include "../EulerTask.h"

class Task_027 : public EulerTask {
public:
	Task_027();
	virtual ~Task_027();
	virtual std::string task() override;
	virtual void cleanup() override;
	virtual void setup() override;
};

#endif /* TASK_027_H_ */
