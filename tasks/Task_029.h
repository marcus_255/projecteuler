/*
 * Task_029.h
 *
 *  Created on: Feb 20, 2020
 *      Author: marcus
 */

#ifndef TASK_029_H_
#define TASK_029_H_

#include "../EulerTask.h"

class Task_029: public EulerTask {
public:
	Task_029();
	virtual ~Task_029();
	virtual void setup() override;
	virtual std::string task() override;
	virtual void cleanup() override;
};

#endif /* TASK_029_H_ */
