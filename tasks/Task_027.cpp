/*
 * Task_027.cpp
 *
 *  Created on: Feb 20, 2020
 *      Author: marcus
 */

#include "Task_027.h"

Task_027::Task_027() :
		EulerTask(27) {
}

Task_027::~Task_027() {
	// TODO Auto-generated destructor stub
}

bool is_prime(unsigned n) {
    if (n == 1)
        return false;

    unsigned i = 2;
    while (i*i <= n)
        if (n % i++ == 0)
            return false;
    return true;
}

void Task_027::setup() {
}

std::string Task_027::task() {
	int lower = -1000;
	int upper = 1000;
	int n_limit = 10000;
	int y;
	int seq_len = 0;
	int max_seq_len = 0;
	std::pair<int, int> best_match;

	for (int a = lower - 1; a < upper; ++a) {
		for (int b = lower; b < upper + 1; ++b) {
			seq_len = 0;
			int n;
			for (n = 0; n < n_limit; ++n) {
				y = n*n + n*a + b;
				if (y < 2 && !seq_len)
					continue;
				else if (y < 2)
					break;
				if (!is_prime(y))
					break;
				seq_len++;
			}
			if (seq_len > max_seq_len) {
				max_seq_len = seq_len;
				best_match = std::make_pair(a, b);
			}
		}
	}

	return std::to_string(best_match.first * best_match.second);
}

void Task_027::cleanup() {
}
