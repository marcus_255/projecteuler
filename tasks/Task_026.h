/*
 * Task_024.h
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#ifndef TASK_026_H_
#define TASK_026_H_

#include "../EulerTask.h"

class Task_026: public EulerTask {
public:
	Task_026();
	virtual ~Task_026();
	virtual std::string task() override;
	virtual void cleanup() override;
	virtual void setup() override;
};

#endif /* TASK_026_H_ */
