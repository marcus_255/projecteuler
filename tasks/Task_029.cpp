/*
 * Task_029.cpp
 *
 *  Created on: Feb 20, 2020
 *      Author: marcus
 */

#include "Task_029.h"

#include <set>
#include <boost/multiprecision/cpp_int.hpp>

using namespace boost::multiprecision;

Task_029::Task_029() :
		EulerTask(29) {
}

Task_029::~Task_029() {
	// TODO Auto-generated destructor stub
}

void Task_029::setup() {
}

int1024_t uint_power(int1024_t x, int1024_t p) {
	if (p == 0)
		return 1;
	if (p == 1)
		return x;

	int1024_t tmp = uint_power(x, p / 2);
	if (p % 2 == 0)
		return tmp * tmp;
	else
		return x * tmp * tmp;
}

std::string Task_029::task() {
	std::set<int1024_t> terms;
	int1024_t a = 100, b = 100;

	for (int1024_t i = 2; i <= a; i++)
		for (int1024_t j = 2; j <= b; j++)
			terms.insert(uint_power(i, j));

	return std::to_string(terms.size());
}

void Task_029::cleanup() {
}
