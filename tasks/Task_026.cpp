/*
 * Task_026.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#include "Task_026.h"

#include <list>
#include <string>

Task_026::Task_026() :
		EulerTask(26) {
}

Task_026::~Task_026() {
}

void Task_026::setup() {
}

std::string fractionToDecString(unsigned n, unsigned d, unsigned max_digits) {
	std::list<char> digits;
	unsigned temp = n;
	unsigned cur_digits = 0;
	while (temp && cur_digits < max_digits) {
		while (temp < d) {
			temp *= 10;
			digits.push_back('0' + (temp / d));
			cur_digits++;
		}
		temp %= d;
	}
	return std::string(digits.begin(), digits.end());
}

std::string getRecurringPattern(std::string sequence, unsigned repeat_count) {
	unsigned seq_len = sequence.length();
	std::string pattern = "";
	bool matched = false;
	for (unsigned i = 1; i < (seq_len / repeat_count) + 1; ++i) {
		pattern = sequence.substr(seq_len - i, i);
		for (unsigned j = 2; j <= repeat_count; ++j) {
			if (sequence.compare(seq_len - (j * i), i, pattern))
				break;

			matched = true;
		}
		if (matched)
			break;
	}
	return matched ? pattern : "";
}

std::string Task_026::task() {
	unsigned limit = 1000;
	unsigned max_dec_str_size = 10000;
	std::pair<unsigned, unsigned> match(0, 0);

	for (unsigned i = 1; i < limit; ++i) {
		std::string dec_str = fractionToDecString(1, i, max_dec_str_size);
		std::string pattern = getRecurringPattern(dec_str, 3);

		if (pattern.length() > match.second)
			match = std::make_pair(i, pattern.length());
	}

	return std::to_string(match.first);
}

void Task_026::cleanup() {
}
