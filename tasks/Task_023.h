/*
 * Task_023.h
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#ifndef TASK_023_H_
#define TASK_023_H_

#include "../EulerTask.h"

class Task_023: public EulerTask {
public:
	Task_023();
	virtual std::string task() override;
	virtual void cleanup() override;
	virtual void setup() override;
};

#endif /* TASK_023_H_ */
