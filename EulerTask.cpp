/*
 * EulerTask.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#include "EulerTask.h"

unsigned EulerTask::task_timeout = 0;

EulerTask::EulerTask(int id) :
		task_id { id } {
}

int EulerTask::run() {
	setup();

	if (task_timeout != 0) {
		// TODO: Implement threaded version here, with join(timeout)
	} else {
		task_result = task();
	}

	cleanup();

	return 0;
}

std::string EulerTask::getResult() {
	return task_timed_out ? "TIMED OUT" : task_result;
}

void EulerTask::setTaskTimeout(unsigned timeout) {
	task_timeout = timeout;
}

EulerTask::~EulerTask() {
}

std::string EulerTask::getTaskName() {
	return ("#" + std::to_string(task_id));
}
