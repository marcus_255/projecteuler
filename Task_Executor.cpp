/*
 * Task_Executor.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#include "Task_Executor.h"

TaskExecutor::TaskExecutor() :
		TaskExecutor(1) {
}

TaskExecutor::TaskExecutor(unsigned threads) :
		max_threads { threads }, tasks_results {} {
}

void TaskExecutor::execute(std::vector<std::shared_ptr<EulerTask>> &tasks) {
	// TODO: use threads to execute tasks in parallel
	for (auto task : tasks) {
		// Measure and store task execution time
		task->run();
		tasks_results.emplace(std::make_pair(task, task->getResult()));
	}
}

void TaskExecutor::printResults() {
	for (auto [task, result] : tasks_results) {
		std::cout << "Task " << task->getTaskName() << " result is " << result << std::endl;
	}
}

TaskExecutor::~TaskExecutor() {
}

