/*
 * Task_Executor.h
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#ifndef TASK_EXECUTOR_H_
#define TASK_EXECUTOR_H_

#include <iostream>
#include <map>
#include <vector>
#include <memory>
#include "EulerTask.h"

class TaskExecutor {
	unsigned max_threads = 1;
	// TODO: Replace tasks_results map with TaskResult object
	//       with designated fields like name, result, execution time etc.
	std::map<std::shared_ptr<EulerTask>, std::string> tasks_results;
public:
	TaskExecutor();
	TaskExecutor(unsigned threads);
	void execute(std::vector<std::shared_ptr<EulerTask>> &tasks);
	void printResults();
	virtual ~TaskExecutor();
};

#endif /* TASK_EXECUTOR_H_ */
