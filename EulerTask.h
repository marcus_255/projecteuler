/*
 * EulerTask.h
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#ifndef EULERTASK_H_
#define EULERTASK_H_

#include <iostream>
#include <string>

class EulerTask {
	static unsigned task_timeout;
	bool task_timed_out = false;
	int task_id = 0;
	std::string task_result = "";
public:
	EulerTask(int id);
	int run();
	std::string getResult();
	std::string getTaskName();
	static void setTaskTimeout(unsigned timeout);

	virtual void setup() = 0;		 // Memory allocation
	virtual std::string task() = 0;  // Task run, may be interrupted
	virtual void cleanup() = 0;      // Memory cleanup, always executed

	virtual ~EulerTask();
};

#endif /* EULERTASK_H_ */
