/*
 * main.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: marcus
 */

#include <iostream>
#include <memory>
#include <vector>

#include "EulerTask.h"
#include "tasks/Task_023.h"
#include "tasks/Task_026.h"
#include "tasks/Task_027.h"
#include "tasks/Task_029.h"
#include "Task_Executor.h"

int main(int argc, char** argv) {

	std::vector<std::shared_ptr<EulerTask>> euler_tasks;
	euler_tasks.push_back(std::make_shared<Task_023>());
	euler_tasks.push_back(std::make_shared<Task_026>());
	euler_tasks.push_back(std::make_shared<Task_027>());
	euler_tasks.push_back(std::make_shared<Task_029>());

	TaskExecutor task_executor;
	task_executor.execute(euler_tasks);
	task_executor.printResults();

	return 0;
}


